import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Note } from '../shared/note.model';
import { NotesService } from '../shared/notes.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Observable, Subject, shareReplay, takeUntil } from 'rxjs';
import { DialogRef, DIALOG_DATA } from '@angular/cdk/dialog';

@Component({
  selector: 'app-note-details',
  templateUrl: './note-details.component.html',
  styleUrls: ['./note-details.component.scss'],
})
export class NoteDetailsComponent implements OnInit, OnDestroy {
  note$?: Observable<Note>;
  noteId?: number;
  new?: boolean;
  destroy$ = new Subject<void>();

  constructor(
    private notesService: NotesService,
    private router: Router,
    private route: ActivatedRoute,
    public dialogRef: DialogRef<string>,
    @Inject(DIALOG_DATA) public data: Note,
  ) {}

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      if (params['id']) {
        this.note$ = this.notesService
          .get(params['id'])
          .pipe(shareReplay(1), takeUntil(this.destroy$));
        this.noteId = params['id'];
        this.new = false;
      } else {
        this.new = true;
      }
    });
  }

  onSubmit(form: NgForm) {
    console.log(form);
    console.log(form.value);
    if (this.new) {
      this.notesService.add(form.value).subscribe((res) => {
        console.log(res);
      });
    } else {
      this.notesService.update(this.noteId!, form.value).subscribe((res) => {
        console.log(res);
      });
    }
    this.dialogRef.close();
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
