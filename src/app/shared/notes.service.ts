import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Note } from './note.model';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { first, tap } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class NotesService {
  BASE_URL = 'http://localhost:8066/notes';

  notes$ = new BehaviorSubject<Note[]>([]);

  constructor(private readonly httpClient: HttpClient) {
    this.getAll().subscribe((notes) => this.notes$.next(notes));
  }

  getAll() {
    return this.httpClient.get<Note[]>(`${this.BASE_URL}/`);
  }

  get(id: number) {
    return this.httpClient.get<Note>(`${this.BASE_URL}/${id}`);
  }

  add(note: Note) {
    return this.httpClient.post<Note>(`${this.BASE_URL}`, note).pipe(
      tap((newNote) => {
        const currentValue = this.notes$.getValue();
        this.notes$.next([...currentValue, newNote]);
      })
    );
  }

  update(id: number, inputNote: Note) {
    return this.httpClient
      .patch<Note>(`${this.BASE_URL}/${id}`, inputNote)
      .pipe(
        tap((updatedNote) => {
          const currentValue = this.notes$.getValue();
          const updatedValue = currentValue.map((note) =>
            {
              console.log('Note: ', note.id + id);
              return note.id == id ? updatedNote : note}
          );
          console.log('Hereeeee: ', updatedValue);
          this.notes$.next(updatedValue);
        })
      );
  }

  delete(id: number) {
    return this.httpClient.delete<boolean>(`${this.BASE_URL}/${id}`).pipe(
      tap(() => {
        const currentValue = this.notes$.getValue();
        const updatedValue = currentValue.filter((note) => note.id !== id);
        this.notes$.next(updatedValue);
      })
    );
  }
}
