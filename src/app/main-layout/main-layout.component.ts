import { Component } from '@angular/core';
import { Dialog } from '@angular/cdk/dialog';
import { NoteDetailsComponent } from '../note-details/note-details.component';

@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.scss']
})
export class MainLayoutComponent {

  constructor(private dialogRef: Dialog) { }

  openDialog() {
    this.dialogRef.open(NoteDetailsComponent);
  }
}
